﻿using System;
using System.Diagnostics;

namespace OAuth
{
#if !Smartphone
    [DebuggerDisplay("{Name}:{Value}")]
#endif
#if !SILVERLIGHT && !WINRT
    [Serializable]
#endif
    public class WebParameter
    {
        public WebParameter(string name, string value)
        {
            if (IsNullOrBlank(name))
                throw new ArgumentException("The parameter name must not be null or empty", "name");

            Name = name;
            Value = value;
        }

        public string Value { get; set; }
        public string Name { get; private set; }

        private static bool IsNullOrBlank(string value)
        {
            return String.IsNullOrEmpty(value) || (!String.IsNullOrEmpty(value) && value.Trim() == String.Empty);
        }
    }
}
